Personal Dotfiles for Linux/Unix
================================

This repository contains personal dotfile configurations for Linux/Unix
machines, which are maintained in a single repository for convenience. They are
managed using [dots] - a really useful dotfile management utility written by
[EvanPurkhiser].

For detailed usage and install instructions, see the [dots] repository; or for
instructions on using specific dotfiles here, refer to each software-specific
config directory. Each file in this repo should be fairly well-documented
in-line.

[dots]: https://github.com/EvanPurkhiser/dots
[EvanPurkhiser]: https://github.com/EvanPurkhiser

Installation & configuration:
-----------------------------

Clone this repo and initialise the local dotfiles store:

```bash
$ git clone https://gitlab.com/deixismou/dotfiles.git
$ source dotfiles/init
```
A new clone of this repo has been installed at `$HOME/.local/etc/`, the the
`dots` script has been installed to `$HOME/.local/lib/dots` and symlinked to
`$HOME/.local/bin/dots`. Providing `$HOME/.local/bin` is in your `$PATH`, you
can view the available install-groups using:

```bash
$ dots groups known
```

Now select your desired dotfile groups and install them to `$HOME/.config/`
using the `dots` command. Make sure to select the `base` group as the
first option, followed by any other groups:

```bash
$ dots groups set base <additional_groups>
$ dots install
```

This command will compile your dotfiles into `$HOME/.config/` and create
symlinks where required (e.g. for Bash, ZSH, and Xorg profiles).

Basic outline of dotfile groups:
--------------------------------

Base-configs are generic configurations which are universally relevant,
regardless of which system. Dotfiles from `common` and `machines` will
extend the configurations defined in `base`.

| Base Group ||
|:----------|:--------------------------------------------------|
| [bash]    | Basic bash config, extended by other groups       |
| [git]     | User global git config                            |
| [gnupg]   | User GnuPG configurationa                         |
| [vim]     | Basic Vim configuration, supports Vim/NeoVim      |
| [zsh]     | Generic ZSH profile, extends from bash profile    |

Common is divided into useful categories, and include configuration
files for software related to that category, and additionally, related
alias and environment variables are added to Bash and ZSH profiles.

| Common Groups ||
|:-------------------|:--------------------------------------------------|
| [development]      | Development tools                                 |
| [virtualisation]   | Docker, virtualbox and vagrant                    |
| [productivity]     | General desktop, messaging, and office apps       |
| [archlinux]        | Archlinux specific aliases and configs            |
| [awesome]          | Awesome WM themes and X11 configs                 |

Machine categories are specific to a particular computer of device;
these groups include workarounds and keybindings that are needed for
a particular device (i.e. laptop media keys, bug workaround scripts)

| Machine Groups  ||
|:----------------|:----------------------|
| [xps13-9350]    | Development laptop    |
| [raspi-osmc]    | Home movie centre     |
| [proli-server]  | Home server           |

[bash]: base/bash/
[git]: base/git/
[gnupg]: base/gnupg/
[vim]: base/vim/
[zsh]: base/zsh/

[development]: common/development/
[virtualisation]: common/virtualisation/
[productivity]: common/productivity/
[archlinux]: common/archlinux/
[awesome]: common/awesome/

[xps13-9350]: machines/xps13-9350/
[raspi-osmc]: machines/raspi-osmc/
[proli-server]: machines/proli-server/

A note on versions:
-------------------

My initial plan was to use a seperate branch for my own version of these
dotfiles, allowing people to use a fairly stable version wholesale; I've since
decided against this because it's just a pain in the arse.

Instead the `master` branch represents the dotfiles I am currently using; expect
it to be hackish, ugly and to change daily. Unless you're me (and I am), using
my dotfiles like this makes absolutely no sense. I do plan to tag them at intervals
when I can reasonably expect them to work. But if my dotfiles break your
toaster, steal your car, or sleep with your wife... well, you have been warned.

Of course, you are welcome to steal things from them.

Credits & kudos:
----------------

Browsing other people's dotfiles is an excellent way to learn new ways of
solving problems like workflow, security or system maintenance and
configuration. A great deal of what you'll find in my dotfiles is a result of my
own searching and discussions with other 'nix users.

I try to give credit for ideas where it's due (both in-file and in the READMEs),
but there is a chance I've borrowed things and forgotten where I found them. If
you see something that you think you should get credit for, drop a post in the
issues section and let me know... If anything, so that I can thank you for it
directly.

And on the subject of giving thanks...

### People with dotfiles that kick-ass:

[Evan Purkhiser]:
    Author of the [dots] utility, who's personal dotfiles are an exceptionally
    well-documented example of how to use it to manage your own repo.

[jessfraz]  (Jessie Frazelle):
    Probably the best collection of desktop-centric [Docker] examples on the
    Internet. Also, check out her [dockerfiles] repo for examples of how to build
    containers for all your special pets.

[amix]  (Amir Salihefendic):
    A fairly extensive vimrc with a full set of plugins. However, I recommend
    taking a closer look at his key mappings for a couple of tidy vim tricks
    that don't require plugins at all. Hint: grep [this file] for
    `You want this`.

[ioerror]  (Jacob Appelbaum):
    For a discussion on hardening and best-practice examples for GnuPG
    configuration, see the article on [RiseUp.net]; The related [gpg.conf] formed
    most of what appears in my own config.

[Evan Purkhiser]: https://github.com/EvanPurkhiser/dots-personal
[dots]: https://github.com/EvanPurkhiser/dots
[jessfraz]: https://github.com/jessfraz/dotfiles
[dockerfiles]: https://github.com/jessfraz/dockerfiles
[Docker]: https://www.docker.com/
[amix]: https://github.com/amix/vimrc
[this file]: https://github.com/amix/vimrc/blob/master/vimrcs/basic.vim
[Riseup.net]: https://riseup.net/en/security/message-security/openpgp/best-practices
[ioerror]: https://github.com/ioerror/duraconf
[gpg.conf]: https://github.com/ioerror/duraconf/blob/master/configs/gnupg/gpg.conf
