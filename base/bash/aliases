#!/bin/bash

# -----------------------------------------------------------------------------
#       BASE SYSTEM ALIASES
# -----------------------------------------------------------------------------

# -------------------
#  Terminal Commands
# -------------------

alias c='clear'

# -------------
#  Text Editor
# -------------

# replace vim with nvim if installed
if hash nvim 2> /dev/null; then
    alias vim='nvim'
fi

# -----------------------
#  Files and Directories
# -----------------------

alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias ......='cd ../../../../..'

alias l='ls -lFh --group-directories-first'   # size,show type,human readable
alias la='ls -lAFh --group-directories-first' # long list,show almost all,show type,human readable
alias ll='ls -l --group-directories-first'    # long list
alias lr='ls -tRFh'      # sorted by date, recursive, show type, human-readable
alias lt='ls -ltFh'      # long list, sorted by date, show type, human-readable
alias ldot='ls -ld .*'   # long list, show dotfiles in CWD
alias lS='ls -1FSsh'     # sort by file size, human-readable
alias lrt='ls -1Fcrt'    # sort by last modified
alias lart='ls -1Fcart'  # sort by last modified, show all

# Use advanced cp command
# https://aur.archlinux.org/packages/advcp/
if hash advcp 2> /dev/null; then
    alias cp='advcp -gp'
    alias sucp='sudo advcp -gp'
fi

# Use advanced mv command
# https://aur.archlinux.org/packages/advcp/
if hash advmv 2> /dev/null; then
    alias mv='advmv -g'
    alias sumv='sudo advmv -g'
fi

alias grep='grep --color'
alias sgrep='grep -R -n -H -C 5 --exclude-dir={.git,.svn,CVS} '

alias t='tail -f'

alias dud='du -d 1 -h'
alias duf='du -sh *'
alias fd='find . -type d -name'
alias ff='find . -type f -name'

alias h='history'
alias hgrep="fc -El 0 | grep"
alias help='man'
alias p='ps -f'
alias sortnr='sort -n -r'
alias unexport='unset'

# ----------------
#  System Control
# ----------------

alias shdn='shutdown -h now'               # Shudown (default)
alias rbt='reboot'                         # Reboot (default)
alias rbtw='grub-reboot 2 && reboot'       # Reboot to Windows
alias spn='systemctl hybrid-sleep'         # Suspend (hibernate on bat empty)
alias hib='systemctl hibernate'            # hibernate

# --------------------
#  Network Interfaces
# --------------------

alias wup='sudo ip link set wlp8s0 up'      # Bring WIFI up
alias wdown='sudo ip link set wlp8s0 down'  # Take WIFI down
alias eup='sudo ip link set enp7s0 up'      # Bring Ethernet up
alias edown='sudo ip link set enp7s0 down'  # Take Ethernet down

alias netl='sudo netctl list'            # List available profiles
alias netst='sudo netctl store'          # Save which profiles are active
alias netrs='sudo netctl restore'        # Load saved profiles
alias netda='sudo netctl stop-all'       # Stops all profiles
alias netu='sudo netctl start ${i}'      # Start a profile
alias netd='sudo netctl stop ${i}'       # Stop a profile
alias netr='sudo netctl restart ${i}'    # Restart a profile
alias nets='sudo netctl switch-to ${i}'  # Switch to a profile
alias neti='sudo netctl status ${i}'     # Show runtime status of a profile
alias neten='sudo netctl enable ${i}'    # Enable the systemd unit for a profile
alias netdi='sudo netctl disable ${i}'   # Disable the systemd unit for a profile
alias netre='sudo netctl reenable ${i}'  # Reenable the systemd unit for a profile
