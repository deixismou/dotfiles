"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" helpers.vim
"
" Description: Provides helper functions for bindings and settings.
"              This file is sourced from vimrc.
"
" Author: 	Corey Mark Bresnan
"
" Version: 	0.2.0 - 27/06/2015
"
" Source: 	https://github.com/cmbresnan/dotfiles/base/vim/functions.d/helpers.vim
"

" Try to get author info from the VCS. The benefit here is that the info is
" relative the the VCS' local settings. (i.e. git in a virtualenv should use
" settings for that repo).
" TODO: add other sources?
function! SetAuthorInfo()
    if executable('git')
        " Ask git for author info. If not set, git returns empty
        let l:author   = system('git config user.name')
        let l:email    = system('git config user.email')
        let l:github   = system('git config github.user')
        let l:website  = system('git config user.website')
    else
        " Leave it blank
        let l:author   = ''
        let l:email    = ''
        let l:github   = ''
        let l:website  = ''
    endif

    " Let's set them as globals
    let g:vim_author  = l:author
    let g:vim_email   = l:email
    let g:vim_github  = l:github
    let g:vim_website = l:website
endfunction

" Strip trailing white space on save, ignoring only TWS-sensitive filetypes.
function! StripTrailingWhitespace()
    " Only strip if the b:noStripeWhitespace variable isn't set
    if exists('b:noStripWhitespace')
        return
    endif
    exe "normal mz"
    %s/\s\+$//ge
    exe "normal `z"
endfunction

autocmd FileType markdown let b:noStripWhitespace=1
autocmd BufWritePre * call StripTrailingWhitespace()

" Return to last edit position when opening files (You want this!)
autocmd BufReadPost *
     \ if line("'\"") > 0 && line("'\"") <= line("$") |
     \   exe "normal! g`\"" |
     \ endif
" Remember info about open buffers on close
set viminfo^=%

function! CmdLine(str)
    exe "menu Foo.Bar :" . a:str
    emenu Foo.Bar
    unmenu Foo
endfunction

function! VisualSelection(direction) range
    let l:saved_reg = @"
    execute "normal! vgvy"

    let l:pattern = escape(@", '\\/.*$^~[]')
    let l:pattern = substitute(l:pattern, "\n$", "", "")

    if a:direction == 'b'
        execute "normal ?" . l:pattern . "^M"
    elseif a:direction == 'gv'
        call CmdLine("vimgrep " . '/'. l:pattern . '/' . ' **/*.')
    elseif a:direction == 'replace'
        call CmdLine("%s" . '/'. l:pattern . '/')
    elseif a:direction == 'f'
        execute "normal /" . l:pattern . "^M"
    endif

    let @/ = l:pattern
    let @" = l:saved_reg
endfunction

" Returns true if paste mode is enabled
function! HasPaste()
    if &paste
        return 'PASTE MODE  '
    en
    return ''
endfunction

" Don't close window, when deleting a buffer
command! Bclose call <SID>BufcloseCloseIt()
function! <SID>BufcloseCloseIt()
    let l:currentBufNum = bufnr("%")
    let l:alternateBufNum = bufnr("#")

    if buflisted(l:alternateBufNum)
         buffer #
    else
         bnext
    endif

    if bufnr("%") == l:currentBufNum
        new
    endif

    if buflisted(l:currentBufNum)
        execute("bdelete! ".l:currentBufNum)
    endif
endfunction
