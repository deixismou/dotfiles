"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" settings.vim
"
" Description: Provides default configuration settings for vim.
"              This file is sourced from vimrc.
"
" Author: 	Corey Mark Bresnan
"
" Version: 	0.2.0 - 27/06/2015
"
" Source: 	https://github.com/cmbresnan/dotfiles/base/vim/plugins.vim
"

""""""""""""""""""""
"  User Interface  "
""""""""""""""""""""

" Scrolloff, min number of screen lines above/below the cursor.
set scrolloff=7

" Wild menu, advanced command-line completion.
set wildmenu

" Ignore auxiliary files when auto-completing
set wildignore+=*~
" Binary executables
set wildignore+=*.exe,*.out,*.app,*.i*86,*.x86_64,*.hex
" Compressed archives
set wildignore+=*.7z,*.bzip,*.gz,*.jar,*.pkg,*.rar,*.tar,*.t\(g\)z,*.xz,*.zip
" C(++) compiled libraries
set wildignore+=*.o,*.ko,*.obj,*.elf,*.gch,*.pch,*.slo,*.lo,*.so,*.dylib,*.dll,*.lai,*.la,*.a,*.lib
" Latex auxiliary/intermediary files
set wildignore+=*.aux,*.out,*.dvi,*.ps,*.eps,*.pdf
" Python binaries and cache
set wildignore+=*.pyc,__pycache__/*,\(.\)\(v\)env/*,*\(.\)egg\(s\|-info\)/*,

" Show ln/col of cursor position on the last visible line.
set ruler

" Height of the command bar
set cmdheight=2

" Hide abandoned buffers instead of unloading them.
set hidden

" Full backspace behaviour (default is only for Vi compatiblity)
set backspace=eol,start,indent
set whichwrap+=<,>,h,l

" Ignore case when using search
set ignorecase

" Override ignorecase when search pattern uses upper case chars.
set smartcase

" Highlight search results.
set hlsearch

" Use incremental matching while typing search patterns.
set incsearch

" Don't redraw while executing macros (performance tweak).
set lazyredraw

" Use regular expression pattern matching.
set magic

" Indicate matching brackets when the text indicatior is over them.
set showmatch
" Length of time to highlight matching brackets (10ths/second).
set mat=2

" Disable error/notification sounds.
set noerrorbells
set novisualbell
set t_vb=

" Partial keycodes and :mappings will time-out if left too long.
set timeout
" length of time to wait for a complete keycode/:mapping.
set timeoutlen=500

" Stop indenting when pasting with mouse, press <F5> in insert mode.
set pastetoggle=<f5>

" Allow mouse interaction ( click-to-cursor, scrollwheel, etc )
set mouse=a

" Show line numbers by default on the left edge of the buffer.
set number

" Show a column highlight at 80 col
set colorcolumn=80

" Show a row highlight at the cursor line
set cursorline

" Always show the status line
set laststatus=2

" Specify the behavior when switching between buffers.
try
  set switchbuf=useopen,usetab,newtab
  set stal=2
catch
endtry

"""""""""""""""""""""
"  Colours & Fonts  "
"""""""""""""""""""""

" Enable syntax highlighting.
syntax enable
set background=dark

" Colour Scheme used for highlighting.
colorscheme solarized

" Set standard language and encoding.
set encoding=utf8

" Use automatic <EOL> detection when reading a line
set fileformats=unix,dos,mac

" Add highlighting support for LESS files.
au BufNewFile,BufRead *.less set filetype=less

" Configure better highlighting for Java
let java_highlight_functions = 1
let java_highlight_all = 1
highlight link javaScopeDecl Statement
highlight link javaType Type
highlight link javaDocTags PreProc

"""""""""""""""""""""""""""""
"  Files, Backups & Undo  "
"""""""""""""""""""""""""""""

" Number of lines to be preserved in undo history
set history=900

" Enable filetype plugins
filetype plugin on
filetype indent on

" Auto-read when a file is changed oustide of Vim
set autoread

" Turn backup off and rely on version control (i.e git).
set nobackup
set nowritebackup
set noswapfile

" Allow taboo to restore tabline titles on session load
set sessionoptions+=globals,tabpages

""""""""""""""""""""""""""""
"  Text, Spaces & Indents  "
""""""""""""""""""""""""""""

" Expand tabs into spaces (for consistancy across editors).
set expandtab

" A tab in front of a line inserts according to 'shiftwidth'.
set smarttab

" <Tab> == 4 spaces.
set shiftwidth=2
set tabstop=2

" Linebreak on 80 characters.
set linebreak
set textwidth=80

" Auto-indent new lines.
set autoindent
" Use smart indenting.
set smartindent

" Don't wrap long lines to fit in the screen width.
set nowrap
