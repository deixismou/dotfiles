Key Mappings Cheat-Sheet
========================

The following is a non-exhaustive list of custom key mappings specific to this
Vim configuration. Some plugin-specific default bindings are included for
reference, however the bulk of Vim's default mappings have been omitted. The
leader key, which has been re-mapped to <kbd>,</kbd> (comma), is included in the
key sequences below.

>Note: This table is for reference only, and is secondary to actually reading
>the [mappings](plugins.vim) and [plugins](plugins.vim) files in the vim config
>directory, since these mappings may change regularly.

| __Key Sequence__ | __Descritpion__ |
| ---: | :--- |
| __Cursor Movement__ ||
| <kbd>ctrl</kbd> + { <kbd>h</kbd> <kbd>j</kbd> <kbd>k</kbd> <kbd>l</kbd> } | Move cursor between windows                                                                                                                                                                  |
| <kbd>ctrl</kbd> + { <kbd>h</kbd> <kbd>j</kbd> <kbd>k</kbd> <kbd>l</kbd> } | Move cursor between windows                                                                                                                                                                  |
| <kbd>0</kbd> | Move cursor to the first non-blank character in the line (equivalent to default <kbd>&#94;</kbd> mapping) |
| <kbd>9</kbd> | Move cursor to the last character in the line (equivalent to default <kbd>$</kbd> mapping) |
| <kbd>,</kbd> <kbd>,</kbd> {motion}             | Trigger EasyMotion for {motion} command (see: [EasyMotion docs](https://github.com/easymotion/vim-easymotion/blob/master/doc/easymotion.txt)) |
| <kbd>,</kbd> <kbd>,</kbd> <kbd>s</kbd>         | EasyMotion move to character target (muti-window) |
| <kbd>,</kbd> <kbd>,</kbd> <kbd>l</kbd>         | EasyMotion move to line target (multi-window) |
| <kbd>,</kbd> <kbd>,</kbd> <kbd>w</kbd>         | EasyMotion move to word target (multi-window) |
| <kbd>g</kbd> <kbd>a</kbd>                      | `normal` or`visual` invokes EasyAlign in respective mode |
| <kbd>alt</kbd> + { <kbd>j</kbd> <kbd>k</kbd> } | `normal` move current line, or `visual` move the currently selected lines, up or down respectively |
| __Code Commenting__ ||
| <kbd>,</kbd> <kbd>;</kbd> <kbd>;</kbd>           | `normal` toggle line comment, `visual` toggle comment of selected text. |
| <kbd>,</kbd> <kbd>;</kbd> <kbd>p</kbd>           | Toggle comment for the current paragraph |
|  `visual` <kbd>,</kbd> <kbd>;</kbd> <kbd>i</kbd> | Comment in-line (some filetypes only) |
|  `normal` <kbd>,</kbd> <kbd>;</kbd> <kbd>r</kbd> | Comment to right of the cursor |
| <kbd>,</kbd> <kbd>;</kbd> <kbd>b</kbd>           | Comment the current code block |
| <kbd>,</kbd> <kbd>;</kbd> <kbd>a</kbd>           | Comment line using syntax of a different filetype |
|| Other functions available (see [tcomment docs](https://github.com/tomtom/tcomment_vim/blob/master/doc/tcomment.txt)) |
| __Buffers and Tabs__ ||
| <kbd>,</kbd> <kbd>b</kbd> <kbd>e</kbd> | Open a new buffer in the same directory as the current buffer |
| <kbd>,</kbd> <kbd>b</kbd> <kbd>d</kbd> | Close the currently active buffer |
| <kbd>,</kbd> <kbd>b</kbd> <kbd>a</kbd> | Close all open buffers |
| <kbd>,</kbd> { <kbd>l</kbd> <kbd>h</kbd> }              | Switch to the next or previous open buffer |
| <kbd>,</kbd> <kbd>t</kbd> <kbd>n</kbd>                  | Open a new tab |
| <kbd>,</kbd> <kbd>t</kbd> <kbd>r</kbd>                  | Rename current tab |
| <kbd>,</kbd> <kbd>t</kbd> { <kbd>l</kbd> <kbd>h</kbd> } | Switch to the next or previous tab |
| <kbd>,</kbd> <kbd>t</kbd> <kbd>c</kbd> | Collapse the currently active tab |
| <kbd>,</kbd> <kbd>t</kbd> <kbd>o</kbd> | Collapse all but the currently active tab (see tabonly) |
| <kbd>,</kbd> <kbd>t</kbd> <kbd>m</kbd> | Call :tabmove and wait for imput: typing { - + 0 $ } followed by <kbd>enter</kbd> will change the order of the current tab in the list to _previous_, _next_, _first_ or _last_ respectively |
| <kbd>,</kbd> <kbd>t</kbd> <kbd>e</kbd> | Open a new tab in the same directory as the current buffer |
| <kbd>,</kbd> <kbd>c</kbd> <kbd>d</kbd> | Change to the current working directory of the active buffer |
| __Saving & Quitting__ ||
| <kbd>,</kbd> <kbd>w</kbd>              | Write buffer (save) |
| <kbd>,</kbd> <kbd>w</kbd> <kbd>q</kbd> | Write buffer (save) & close / quit |
| <kbd>,</kbd> <kbd>q</kbd>              | Close / Quit |
| <kbd>,</kbd> <kbd>s</kbd> <kbd>s</kbd> | Load a saved session |
| <kbd>,</kbd> <kbd>s</kbd> <kbd>w</kbd> | Save active session |
| <kbd>,</kbd> <kbd>s</kbd> <kbd>d</kbd> | Delete active session |
| <kbd>,</kbd> <kbd>s</kbd> <kbd>q</kbd> | Close active session |
| __Fugative Git Bindings__ ||
| <kbd>,</kbd> <kbd>g</kbd> <kbd>b</kbd> | Open a vertical split with the output of `git blame` for the current buffer |
| <kbd>,</kbd> <kbd>g</kbd> <kbd>c</kbd> | Execute `git commit` |
| <kbd>,</kbd> <kbd>g</kbd> <kbd>d</kbd> | Open horizontal split with the output of `git diff` for the current buffer |
| <kbd>,</kbd> <kbd>g</kbd> <kbd>p</kbd> | Execute `git push` |
| <kbd>,</kbd> <kbd>g</kbd> <kbd>r</kbd> | Read the current committed state of the file into the buffer |
| <kbd>,</kbd> <kbd>g</kbd> <kbd>s</kbd> | Open a horizontal split pane with the output of `git status` |
| <kbd>,</kbd> <kbd>g</kbd> <kbd>w</kbd> | Write to the buffer's filepath and stage the result |
| <kbd>,</kbd> <kbd>g</kbd> <kbd>-</kbd> | Execute `git stash` and refresh the buffer |
| <kbd>,</kbd> <kbd>g</kbd> <kbd>+</kbd> | Execute `git stash pop` and refresh the buffer |
| __Search and Grep Functions__ ||
| <kbd>space</kbd>                       | Search forward (equivalent to default <kbd>/</kbd> mapping) |
| <kbd>ctrl</kbd> <kbd>space</kbd>       | Search backwards (equivalent to default <kbd>?</kbd> mapping) |
| <kbd>&#42;</kbd>                       | `normal` search word currently under cursor, `visual` search selection |
| <kbd>#</kbd>                           | As above, but will search backwards |
| <kbd>,</kbd> <kbd>enter</kbd>          | Remove highlighting from previous search |
| <kbd>,</kbd> <kbd>a</kbd> <kbd>g</kbd> | Standard ack search |
| <kbd>,</kbd> <kbd>a</kbd> <kbd>a</kbd> | Additive ack search |
| <kbd>,</kbd> <kbd>a</kbd> <kbd>f</kbd> | Ack filename pattern search |
| <kbd>,</kbd> <kbd>a</kbd> <kbd>w</kbd> | Ack search within the visible window space |
| <kbd>,</kbd> <kbd>r</kbd>              | Calls replace for the the currently selected text |
| `visual` <kbd>,</kbd> <kbd>r</kbd>     | Call replace for the currently selected text |
| <kbd>,</kbd> <kbd>c</kbd> <kbd>c</kbd> | Display results of vimgrp in a cope window |
| <kbd>,</kbd> <kbd>n</kbd>              | Move to the next item in the quickfix list |
| <kbd>,</kbd> <kbd>p</kbd>              | Move to the previous item in the quickfix list |
| __Specll Checking__ ||
| <kbd>,</kbd> <kbd>s</kbd> <kbd>c</kbd> | Toggle spell checking on and off |
| <kbd>,</kbd> <kbd>s</kbd> <kbd>n</kbd> | Move to the next item in the spellcheck list |
| <kbd>,</kbd> <kbd>s</kbd> <kbd>p</kbd> | Move to the previous item in the spellcheck list |
| <kbd>,</kbd> <kbd>s</kbd> <kbd>a</kbd> | Add the word under the cursor as a "good" word in the spellfile |
| <kbd>,</kbd> <kbd>s</kbd> <kbd>?</kbd> | Suggest spellings for the word at the current cursor position |
| __Miscellaneous__ ||
| <kbd>,</kbd> <kbd>m</kbd>              | Remove `^M` Windows encodings |
| <kbd>,</kbd> <kbd>e</kbd>              | Open an empty throw-away buffer for quick notes |
| <kbd>,</kbd> <kbd>p</kbd> <kbd>p</kbd> | Toggle `paste` mode on or off |
| <kbd>,</kbd> <kbd>r</kbd> <kbd>c</kbd> | reload the vim configuration file |

## Understanding the key sequence notation

| __Key Sequence Example__ | __Instructions__ |
| ---: | :--- |
| <kbd>ctrl</kbd> + <kbd>j</kbd>                  | Press <kbd>ctrl</kbd> + <kbd>j</kbd> at the same time |
| <kbd>ctrl</kbd> + { <kbd>j</kbd> <kbd>k</kbd> } | Press <kbd>ctrl</kbd> and one of the following keys at the same time (i.e <kbd>ctrl</kbd> + <kbd>j</kbd> OR <kbd>ctrl</kbd> + <kbd>k</kbd> ) |
| <kbd>,</kbd> <kbd>,</kbd> {motion}              | Press <kbd>,</kbd> <kbd>,</kbd> followed by [a standard vim motion command](http://vimdoc.sourceforge.net/htmldoc/motion.html) |
| <kbd>,</kbd> <kbd>w</kbd>                       | Press <kbd>,</kbd> then <kbd>w</kbd> in quick sequence, one after the other |
| `visual` <kbd>,</kbd> <kbd>r</kbd>              | Key sequence is specific to `visual` mode |
