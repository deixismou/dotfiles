"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" plugins.vim
"
" Description: Provides plugin imports for Vundle and plugin-specific settings.
"              This file is sourced from vimrc.
"
" Author: 	Corey Mark Bresnan
"
" Version: 	0.2.0 - 27/06/2015
"
" Source: 	https://github.com/cmbresnan/dotfiles/base/vim/plugins.vim
"
"""""""""""""""""""""""""""
"  Vundle Initialisation  "
"""""""""""""""""""""""""""

call vundle#begin("$XDG_CONFIG_HOME/vim/bundle")

" All plugins are called with: git-user/repo-name
" Vundle is called first, it will manage itself
Plugin 'gmarik/Vundle.vim'

" Ack
" Run ack commands from Vim, shows the results in quickfix window.
Plugin 'mileszs/ack.vim'

" Airline
"   A light-weight powerline implimentation for Vim's statusbar
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'

" Auto-Pairs
"   Automatic insert, close or delete brackets, parens, quotes.
Plugin 'jiangmiao/auto-pairs'

" CtrP
"   Full path fuzzy file, buffer, mru, tag, ... finder for Vim.
Plugin 'ctrlpvim/ctrlp.vim'

" Easy Align
"   Allows fast alignment of lines around operators, tabs, etc
 Plugin 'junegunn/vim-easy-align'

" Easy Motion
"   Allows highlighted, follow-style cursor motions.
Plugin 'easymotion/vim-easymotion'

" Fugitive
"   Git bindings and functions for Vim
Plugin 'tpope/vim-fugitive'
Plugin 'shumphrey/fugitive-gitlab.vim'

" GitGutter
"   Shows a git diff in the 'gutter' (sign column).
Plugin 'airblade/vim-gitgutter'

" vim-gnupg
" Transparent editing of gpg encrypted files
Plugin 'jamessan/vim-gnupg'

" vim-prettier
" An opinionated code formatter with support for many languages
Plugin 'prettier/vim-prettier'

" Solarized
"   Syntax highlighting scheme by Ethan Schoonover
Plugin 'altercation/vim-colors-solarized'

" Startify
"   A useful startup screen with file and session control features
Plugin 'mhinz/vim-startify'

" Surround
"   Easily delete, change and add parentheses, quotes, tags, etc in pairs.
Plugin 'vim-scripts/surround.vim'

" Syntastic
"   Language-specific syntax checking
Plugin 'vim-scripts/Syntastic'
Plugin 'sudar/vim-arduino-syntax' " arduino-specific syntax

" Taboo
" Adds utilities for managing then tabline
Plugin 'gcmt/taboo.vim'

" Tcomment
" Easy-to-use, file-sensible commenting with embedded syntax handling.
Plugin 'tomtom/tcomment_vim'

" UltiSnips
"   Powerful code snippets manager, with clever expansion.
Plugin 'SirVer/ultisnips'
Plugin 'honza/vim-snippets'
Plugin 'sudar/vim-arduino-snippets' " arduino-specific snippets

" YouCompleteMe
"   Dynamic code completion plugin
Plugin 'Valloric/YouCompleteMe'

" Devicons
"   Developer icons for use with patched Nerd Fonts
"   NOTE: Must be called after Airline, CtlP and Startify
Plugin 'ryanoasis/vim-devicons'

" All plugins called before this line.
call vundle#end()

""""""""""""""""""""""""""""""
"  Plugin-Specific Settings  "
""""""""""""""""""""""""""""""

" Ack: if The Silver Searcher is present, use it as the default grepping tool
" Otherwise, fall back on the standard ack command
if executable('ag')
  let g:ackprg = 'ag --vimgrep'
endif

" Airline: Statusline Appearance
let g:airline_powerline_fonts = 1               " Use special formatting chars
let g:airline#extensions#tabline#enabled = 1    " Display the buffer list

" CtrP: searches VCS tracked files, fallback to 'find foo -type f'
" These commands include untracked files, which implies a performance hit.
" To search only tracked files use the following:
"       1: ['.git', 'cd %s && git ls-files'],
"       2: ['.hg', 'hg --cwd %s locate -I .'],
"
let g:ctrlp_user_command = {
    \ 'types': {
        \ 1: ['.git', 'cd %s && git ls-files -co --exclude-standard'],
        \ 2: ['.hg', 'hg --cwd %s status -numac -I . $(hg root)'],
    \ },}

" CtrP: search path mode:
" 'c' - the directory of the current file.
" 'r' - the nearest ancestor that contains one of these directories or files: .git .hg .svn .bzr _darcs
" 'a' - like c, but only if the current working directory outside of CtrlP is not a direct ancestor of the directory of the current file.
" 0 or '' (empty string) - disable this feature.
let g:ctrlp_working_path_mode = 'ra'

" CtrP: opens new files in the current window
let g:ctrlp_open_new_file = 'r'

" CtrP: opens multiple files in the current window then buffers
let g:ctrlp_open_multiple_files = '1rji'

" CtrP: overwrite the Startify window
let g:ctrlp_reuse_window = 'startify'

" EasyAlign: Start interactive EasyAlign in visual mode
xmap ga <Plug>(EasyAlign)

" EasyAlign: Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)

" Prettier: open quickfix on parsing error
let g:prettier#quickfix_enabled = 1

" Prettier: don't autofucus the quickfix on parsing error
let g:prettier#quickfix_auto_focus = 0

" Solarized: Colour Scheme
let g:solarized_visibility="low"    " Reduce visibility of special chars
let g:solarized_termtrans=0         " Disable background transparency
" let g:solarized_bold=0            " Disable bold typeface
" let g:solarized_underline=0       " Disable underlined typeface
" let g:solarized_italic=0          " Disable italic typeface

" Gloabal session directory
let g:startify_session_dir = "$VIM_SESSION_DIR"

" Shut the fuck up, stupid cow!!
let g:startify_custom_header = []

" Don't show `quit` and `empty buffer` options
let g:startify_enable_special = 0

" Automatically load Session.vim if in CWD
let g:startify_session_autoload = 1

" Don't persist sessions; allows sessions to be used as 'tasks modes'.
" We can still save sessions manually.
let g:startify_session_persistence = 0

" Don't CD to the file directory
let g:startify_change_to_dir = 0

" Do CD to the VCS root
let g:startify_change_to_vcs_root = 1

" Delete buffers when switching sessions
let g:startify_session_delete_buffers = 1

" Order of the menu lists in Startify
let g:startify_list_order = [
      \ ['   Sessions:'],
      \ 'sessions',
      \ ['   LRU:'],
      \ 'files',
      \ ['   LRU within this dir:'],
      \ 'dir',
      \ ['   Bookmarks:'],
      \ 'bookmarks',
      \ ]

" Bookmarks to show in Startify
let g:startify_bookmarks = [
                \ '~/.local/etc/base/vim/mappings.vim',
                \ '~/.local/etc/base/vim/plugins.vim',
                \ '~/.local/etc/base/bash/aliases',
                \ '~/.local/etc/base/bash/environment',
                \ '~/.local/etc/base/zsh/aliases.zsh',
                \ ]

" stop taboo from managing the tabline so it plays nice with airline
let g:taboo_tabline = 0

" format tabline appearance
let g:taboo_renamed_tab_format = '%l%m'
let g:taboo_modified_tab_flag = '+'

" Set tcomment 'normal' mode leader
let g:tcomment_mapleader2 = '<leader>;'

" UltiSnips Editing and Expansion
let g:UltiSnipsExpandTrigger="<c-j>"    " Snippet expansion trigger
let g:UltiSnipsSnippetsDir="$XDG_CONFIG_HOME/vim/UltiSnips" " Custom snippets

" BUG: only seems to work with an absolute path.
"      see: https://github.com/SirVer/ultisnips/issues/948
let g:UltiSnipsSnippetDirectories = ['/home/cmbresnan/.config/vim/UltiSnips','UltiSnips']

" Set author info used in some code snippets (e.g. docstrings, headers)
let g:snips_author = g:vim_author
let g:snips_email  = g:vim_email
let g:snips_github = g:vim_github
