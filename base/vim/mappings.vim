"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" mappings.vim
"
" Description: Provides default keybindings and mappings for Vim.
"              This file is sourced from vimrc.
"
" Author: 	Corey Mark Bresnan
"
" Version: 	0.2.0 - 27/06/2015
"
" Source: 	https://github.com/cmbresnan/dotfiles/base/vim/plugins.vim
"
""""""""""""""""""""""
"  General Mappings  "
""""""""""""""""""""""

" Set the leader key
let mapleader = ","
let g:mapleader = ","

" Fast saving
nmap <leader>w :w!<cr>
nmap <leader>wq :wq!<cr>
nmap <leader>q :q<cr>

" Starify session mappings
map <leader>ss :SLoad<CR>
map <leader>sw :SSave<CR>
map <leader>sd :SDelete<CR>
map <leader>sq :SClose<CR>

"""""""""""""""""""""""""""
"  Visual Mode Selection  "
"""""""""""""""""""""""""""

" Pressing * or # searches for the current selection
" Super useful! borrowed from https://github.com/amix/vimrc
vnoremap <silent> * :call VisualSelection('f')<CR>
vnoremap <silent> # :call VisualSelection('b')<CR>

" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)

" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)<Paste>

""""""""""""""""""""""""""""
"  Cursor Movement & Text  "
""""""""""""""""""""""""""""
"
" Treat long lines as break lines (useful when moving around in them)
map j gj
map k gk

" Remap VIM 0 to first non-blank character
map 0 ^
" Remap VIM 9 to last last characher
map 9 $

" Move a line of text using ALT+[jk]...
nmap <M-j> mz:m+<cr>`z
nmap <M-k> mz:m-2<cr>`z
vmap <M-j> :m'>+<cr>`<my`>mzgv`yo`z
vmap <M-k> :m'<-2<cr>`>my`<mzgv`yo`z
" ...or Comamnd+[jk] on mac
if has("mac") || has("macunix")
    nmap <D-j> <M-j>
    nmap <D-k> <M-k>
    vmap <D-j> <M-j>
    vmap <D-k> <M-k>
endif

" Smart way to move between windows
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

" Easymotion multi-window support (even smarter way to move between windows)
" Move to {char}
map  <Leader><Leader>f <Plug>(easymotion-bd-f)
nmap <Leader><Leader>f <Plug>(easymotion-overwin-f)
" Move to line
map <Leader><Leader>l <Plug>(easymotion-bd-jk)
nmap <Leader><Leader>l <Plug>(easymotion-overwin-line)
" Move to word
map  <Leader><Leader>w <Plug>(easymotion-bd-w)
nmap <Leader><Leader>w <Plug>(easymotion-overwin-w)

" Override tcomment default toggle. The rest of the mappings should play nicely.
noremap <leader>;; :TComment<cr>
xnoremap <leader>;; :TCommentMaybeInline<cr>

"""""""""""""""""""""""""""""
"  Buffers, Tabs & Windows  "
"""""""""""""""""""""""""""""

" Open a new buffer with the current buffer's directory path
map <leader>be :edit <c-r>=expand("%:p:h")<cr>/

" Close the current buffer (SEE: functions.d/helpers.vim)
map <leader>bd :Bclose<cr>

" Close all the buffers
map <leader>ba :bufdo bd<cr>

" Next and previous buffer
map <leader>l :bnext<cr>
map <leader>h :bprevious<cr>

" Useful mappings for managing tabs
map <leader>tn :TabooOpen new tab<cr>
map <leader>tr :TabooRename<space>
map <leader>to :tabonly<cr>
map <leader>tc :tabclose<cr>
map <leader>tm :tabmove
map <leader>tl :tabnext<cr>
map <leader>th :tabprevious<CR>

" Opens a new tab with the current buffer's directory path
map <leader>te :tabedit <c-r>=expand("%:p:h")<cr>/

" Switch CWD to the directory of the open buffer
map <leader>cd :cd %:p:h<cr>:pwd<cr>

""""""""""""""""""""""""""""""""""
"  (fu)Git(ive) Version Control  "
""""""""""""""""""""""""""""""""""

nnoremap <Leader>gb :Gblame<CR>
nnoremap <Leader>gc :Gcommit<CR>
nnoremap <Leader>gd :Gdiff<CR>
nnoremap <Leader>gp :Git push<CR>
nnoremap <Leader>gr :Gread<CR>
nnoremap <Leader>gs :Gstatus<CR>
nnoremap <Leader>gw :Gwrite<CR>
nnoremap <Leader>g- :Silent Git stash<CR>:e<CR>
nnoremap <Leader>g+ :Silent Git stash pop<CR>:e<CR>

"""""""""""""""""""""""""""""""""""
"  Vimgrep, Searching & Quickfix  "
"""""""""""""""""""""""""""""""""""

" Map <Space> to / (search) and Ctrl-<Space> to ? (backwards search)
map <space> <Plug>(easymotion-sn)
omap <space> <Plug>(easymotion-tn)
map  n <Plug>(easymotion-next)
map  N <Plug>(easymotion-prev)

" Disable highlight when <leader><cr> is pressed
map <silent> <leader><cr> :noh<cr>

" Ack for inter-file pattern
map <leader>ag :Ack<space>

" Ack for inter-file pattern
map <leader>aa :AckAdd<space>

" Ack for filename pattern
map <leader>af :AckFile<space>

" Ack in currently visible window
map <leader>aw :AckWindow<space>

" Search and replace the selected text
vnoremap <silent> <leader>r :call VisualSelection('replace')<CR>

" Open the Quickfix window
map <leader>cc :botright cw<cr>

" Go to the next Quickfix result
map <leader>n :cn<cr>

" Go to the previous Quickfix result
map <leader>p :cp<cr>

""""""""""""""""""""
"  Spell Checking  "
""""""""""""""""""""

" Toggle spell checking on and off
map <leader>sc :setlocal spell!<cr>

" Shortcuts using <leader>
map <leader>sn ]s
map <leader>sp [s
map <leader>sa zg
map <leader>s? z=

"""""""""""""""""""
"  Miscellaneous  "
"""""""""""""""""""

" Change the default Prettier mapping
nmap <Leader>py <Plug>(Prettier)

" Remove the Windows ^M - when the encodings gets messed up
noremap <Leader>m mmHmt:%s/<C-V><cr>//ge<cr>'tzt'm

" Quickly open a buffer for scribble
map <leader>e :e ~/buffer<cr>

" Toggle paste mode on and off
map <leader>pp :setlocal paste!<cr>

" Open help search prompt
map <f1> :help<space>

" Open the UltiSnips editor buffer
map <f2> :UltiSnipsEdit<cr>

" Reload the vimrc file
map <f5> :so %<cr>

" Re-install dotfiles
map <f6> :! dots install<cr>

" Use light colourscheme
map <f11> :set background=dark<cr>
map <f12> :set background=light<cr>
